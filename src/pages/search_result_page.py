from src.core.core_page import CorePage
from src.tools.config import Config


class SearchResultPage(CorePage):

    def open(self):
        self.driver.get(Config.APP_URL + '/search')



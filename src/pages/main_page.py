from selenium.common.exceptions import TimeoutException, StaleElementReferenceException, NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from src.core.core_page import CorePage
from src.pages.search_result_page import SearchResultPage


class MainPage(CorePage):
    flight_heading_locator = (By.CSS_SELECTOR, '#home .flights-tab')
    search_button = (By.CSS_SELECTOR, '#search-container button[ng-click="searchFlights()"')
    flight_from = (By.CSS_SELECTOR, 'input[aria-labelledby="label-airport-selector-from"]')
    flight_to = (By.CSS_SELECTOR, 'input[aria-labelledby="label-airport-selector-to"]')

    ''' first method of checking if element is present - by using find_elements which returns an array regardless 
    presence of the element - if not found then it`ll return an empty array'''

    def is_search_heading_present(self):
        return self.is_element_present(self.driver.find_elements(*self.flight_heading_locator))

    ''' Search for elements by directly defining Tuple'''

    def is_search_button_present(self):
        return self.is_element_present(self.driver.find_elements(By.CSS_SELECTOR,
                                                                 '#search-container button[ng-click="searchFlights()"'))

    ''' by using explicit wait we wait for the element - if not found exception will be thrown - thus we can exit with
    False'''

    def is_origin_input_present(self):
        try:
            WebDriverWait(self.driver, self.timeout).until(
                EC.presence_of_element_located(self.flight_from)
            )
            return True
        except(TimeoutException, StaleElementReferenceException):
            return False

    def is_destination_input_present(self):
        return self.is_element_present(self.driver.find_elements(self.flight_heading_locator[0], self.flight_heading_locator[1]))

    def get_flight_from_input(self):
        try:
            return self.driver.find_element(*self.flight_from)
        except NoSuchElementException:
            return None

    ''' this shows an important concept of page_objects
    once an action redirects/opens new tab, etc and we need to continue test
    we should return an instance of the page we`re going to be redirected to
    and in steps continue to work with this new object'''

    def click_search(self):
        self.driver.find_element(*self.search_button).click()
        return self.navigate_to_page(SearchResultPage)

    def fill_origin(self, origin):
        if self.is_origin_input_present:
            input_element = self.driver.find_element(*self.flight_from)
            input_element.clear()
            input_element.send_keys(origin + '\n')

    def fill_destination(self, destination):
        if self.is_destination_input_present:
            input_element = self.driver.find_element(*self.flight_to)
            input_element.clear()
            input_element.send_keys(destination+ '\n')
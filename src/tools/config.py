import configparser
import selenium.webdriver as webdriver


class Config(object):
    """
    Config class for storing values from behave.ini and browser types.
    """
    browser_types = \
        dict(chrome=webdriver.Chrome, firefox=webdriver.Firefox, ie=webdriver.Ie, phantomjs=webdriver.PhantomJS)
    driver_parameters = \
        dict(chrome=['src/resources/chromedriver.exe'],
             firefox=[None, None, 60, None, None, 'src/resources/geckodriver.exe'],
             ie=[webdriver.Ie],
             phantomjs=[])
    config = configparser.ConfigParser()
    config.read('behave.ini')

    BROWSER = config.get('behave.selenium', 'browser').lower()
    TIMEOUT = config.getint('behave.selenium', 'timeout')
    APP_URL = config.get('behave.selenium', 'url')
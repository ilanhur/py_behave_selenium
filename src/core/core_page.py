# import traceback
# from selenium.common.exceptions import TimeoutException, StaleElementReferenceException
from selenium.webdriver.chrome.webdriver import WebDriver
# from selenium.webdriver.support import expected_conditions as EC
# from selenium.webdriver.support.wait import WebDriverWait

from src.tools.config import Config


class CorePage(object):

    def __init__(self, context):
        self.context = context
        self.driver: WebDriver = context.driver
        self.baseUrl = Config.APP_URL
        self.timeout = Config.TIMEOUT

    ''' this concept is too comlpex for now - this method is attached to each variable call on an object
    In other words will be called when variable is requested and by using locator Tuple will try to get the element'''
    # def __getattr__(self, what):
    #     try:
    #         locator = self.what
    #         try:
    #             return WebDriverWait(self.driver, self.timeout).until(
    #                 EC.presence_of_element_located(locator)
    #             )
    #         except(TimeoutException, StaleElementReferenceException):
    #             traceback.print_exc()
    #
    #         try:
    #             return WebDriverWait(self.driver, self.timeout).until(
    #                 EC.visibility_of_element_located(locator)
    #             )
    #         except(TimeoutException, StaleElementReferenceException):
    #             traceback.print_exc()
    #         return None
    #     except AttributeError:
    #         return None

    @staticmethod
    def is_element_present(elements):
        if len(elements) > 0:
            return True
        else:
            return False

    def open(self):
        self.driver.get(self.baseUrl)

    def get_page_title(self):
        return self.driver.title

    def navigate_to_page(self, page_object):
        return page_object(self.context)
from behave import fixture, use_fixture

from src.tools.config import Config


def before_all(context):
    if context.config.userdata:
        Config.BROWSER = context.config.userdata.get('browser', Config.BROWSER).lower()
        Config.APP_URL = context.config.userdata.get('url', Config.APP_URL).lower()
        Config.TIMEOUT = context.config.userdata.getint('timeout', Config.TIMEOUT)

def before_scenario(context, scenario):
    if 'webUI' in scenario.tags:
        use_fixture(initialize_webdriver, context)

    if 'api' in scenario.tags:
        print("TODO")


def after_feature(context, feature):
    if 'webUI' in feature.tags:
        try:
            context.driver.quit()
        except Exception as ex:
            print("The teardown of the driver object is impossible since it was not created due to: %s", str(ex))

    if 'api' in feature.tags:
        try:
            print("TODO")
        except Exception as ex:
            print("The teardown of the api_client object is impossible since it was not created due to: %s", str(ex))


@fixture
def initialize_webdriver(context):
    context.driver = Config.browser_types[Config.BROWSER](*Config.driver_parameters[Config.BROWSER])
    context.driver.implicitly_wait(Config.TIMEOUT)
    context.driver.set_page_load_timeout(Config.TIMEOUT)
    context.driver.maximize_window()
    yield context.driver
    # -- CLEANUP-FIXTURE PART:
    context.driver.quit()

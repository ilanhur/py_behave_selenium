import src
from src.core import core_test


def before_all(context):
    core_test.before_all(context)


def before_feature(context, feature):
    core_test.after_feature(context, feature)


def before_scenario(context, scenario):
    core_test.before_scenario(context, scenario)


def after_feature(context, feature):
    core_test.after_feature(context, feature)

Feature: Check search widget

  @webUI
  Scenario: Check if search widget is present
    Given user is on Ryanair website
    When page is loaded
    Then flight toggle button should be present method 1
    Then flight toggle button should be present method 2

  @webUI
  Scenario: User tries to find a flight from Warsaw to London for less than 100zl (negative test - we expect it to fail)
    Given user is on Ryanair website
    When page is loaded
    Then User types origin to Warszawa-Modlin
    Then User types destination to Londyn-Stansted
#      Then user selects first available flight date
#      Then user selects first available return date
    Then user clicks search
#      Then user is redirected to search results page
#      Then there are flights listed in results
#      Then there is at least one flight for less than '100' zł

    @webUI
  Scenario: User tries to find a flight from Warsaw to London for more than 300zl (positive test - should pass)
    Given user is on Ryanair website
    When page is loaded
    Then User types origin to Warszawa-Modlin
    Then User types destination to Londyn-Stansted
#      Then user selects first available flight date
#      Then user selects first available return date
    Then user clicks search
#      Then user is redirected to search results page
#      Then there are flights listed in results
#      Then there is at least one flight for more than '300' zł
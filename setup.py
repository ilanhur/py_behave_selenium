from setuptools import setup

setup(name="Selenium workshop",
      version="0.1",
      description="Basic structure and ideas for testing with PY and selenium",
      author="Tomasz Lewarski",
      author_email="tomasz.lewarski@pl.abb.com",
      install_requires=[
          'behave >= 0.1',
          'selenium >= 0.1',
          'pyhamcrest >= 0.1',
          'pytest-allure-adaptor >= 0.1',
          'configparser >= 0.1',
          'timeunit >= 0.1'
      ])

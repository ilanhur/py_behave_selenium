from behave import given, when, then
from hamcrest import *
from selenium.webdriver.remote.webelement import WebElement

from src.pages.main_page import MainPage


@given("user is on Ryanair website")
def user_enters_lufthansa_page(context):
    context.main_page: MainPage = MainPage(context)
    context.main_page.open()


@when("page is loaded")
def page_is_loaded(context):
    assert_that(context.main_page.get_page_title(),
                has_string('Oficjalna Strona Ryanair | Tanie Loty | Bilety lotnicze'))


@then("flight toggle button should be present method 1")
def check_if_search_flight_button_is_present(context):
    element: WebElement = context.main_page.driver.find_element(*context.main_page.flight_heading_locator)
    assert_that(element, not_none())


@then("flight toggle button should be present method 2")
def check_if_search_flight_button_is_present_2(context):
    assert context.main_page.is_search_heading_present()


@then("user clicks search")
def click_on_search_button(context):
    assert context.main_page.is_search_button_present()
    ''' here we perform click and expect context.search_page to be provided '''
    context.search_page = context.main_page.click_search()
    context.search_page.open()

@then("Then user is redirected to search results page")
def check_if_page_did_redirect(context):
    assert_that(context.driver.title, has_string(''))


@then("User types origin to {origin}")
def user_fills_origin(context, origin):
    context.main_page.fill_origin(origin)


@then("User types destination to {destination}")
def user_fills_destination(context, destination):
    context.main_page.fill_destination(destination)
